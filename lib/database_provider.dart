import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static Future<Database>? _database;
  static Future<Database> get database {
    return _database ?? initDB();
  }

  static Future<Database> initDB() async {
    WidgetsFlutterBinding.ensureInitialized();
    _database = openDatabase(
      join(await getDatabasesPath(), 'doggie_database.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)',
        );
      },
      version: 1,
    );
    return _database as Future<Database>;
  }
}

class CatDatabaseProvider {
  static Future<Database>? _catdatabase;
  static Future<Database> get database {
    return _catdatabase ?? initDB();
  }

  static Future<Database> initDB() async {
    WidgetsFlutterBinding.ensureInitialized();
    _catdatabase = openDatabase(
      join(await getDatabasesPath(), 'cat_database.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE cats(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)',
        );
      },
      version: 1,
    );
    return _catdatabase as Future<Database>;
  }
}
